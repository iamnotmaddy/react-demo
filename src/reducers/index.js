import uuid from "react-uuid";

export default (state = [], action) => {
    switch (action.type) {
        case "ADD_TO_LIST" : {
            return state.concat({...action.product, id: uuid()});
        }
        case "AMOUNT_CHANGE": {
            return state.map(obj => obj.id === action.id ? {...obj, 'amount': action.amount} : obj)
        }
        case "DELETE_FROM_LIST": {
            return state.filter(obj => obj.id !== action.id);
        }
        case "RECEIVED_DATA": {
            return action.data.map(obj => ({...obj, id: uuid()}));
        }
        case "DATA_REQUEST_ERROR": {
            console.log(action.error);
            return state;
        }
        default:
            return state;
    }
};