import React from 'react';
import './static/style/App.css';
import Header from "./component/common/Header";
import ProductList from "./component/right-side/ProductList";
import Inputs from "./component/left-side/Inputs";


const App = () =>
    (
        <div className="App">
            <Header/>
            <div className="SplitPane">
                <div className="SplitPane-left">
                    <Inputs/>
                </div>
                <div className="SplitPane-right">
                    <ProductList/>
                </div>
            </div>
        </div>
    );

export default App;
