import React, {useState} from 'react';
import {Icons as icons} from '../common/Icons'
import MyButton from "./Button";


const ProductIconMenu = ({currentIconIndex, onNewIconSelected}) => {

    const [showIconMenu, setShowIconMenu] = useState(false);

    const showIcon = (index) => {
        return icons[index].icon;
    };

    const onIconSelected = (index) => {
        setShowIconMenu(false);
        onNewIconSelected(index);
    };

    const toggleMenu = () => {
        setShowIconMenu(!showIconMenu);
    };

    return (
        <div>
            <MyButton buttonHandler={toggleMenu} buttonClass="IconEntry"
                      buttonContent={showIcon(currentIconIndex)}/>

            {showIconMenu &&
            <div className="IconMenu">
                {icons.map((entry, index) => {
                    return (<MyButton key={index} buttonClass="IconMenuEntry"
                                      buttonHandler={() => onIconSelected(index)}
                                      buttonContent={entry.icon}/>
                    )
                })}
            </div>
            }
        </div>
    )
};

export default ProductIconMenu;
