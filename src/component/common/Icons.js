import React from "react";
import {FaBitcoin, FaGulp, FaGratipay} from "react-icons/fa";
import {GoBug} from "react-icons/go";

export const Icons = [
    {
        icon: <GoBug size={100}/>
    },
    {
        icon: <FaBitcoin size={100}/>
    },
    {
        icon: <FaGulp size={100}/>
    },
    {
        icon: <FaGratipay size={100}/>
    },

];
