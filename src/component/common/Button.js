import React from 'react';

const MyButton = ({buttonClass, buttonHandler, isDisabled, buttonContent}) => (
    <button className={buttonClass} onClick={buttonHandler}
            disabled={isDisabled}
    >{buttonContent}</button>
);

export default MyButton;
