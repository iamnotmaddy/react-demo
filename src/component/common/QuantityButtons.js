import React from 'react';
import MyButton from "./Button";


const QuantityButtons = ({amount, onValueChange}) => {

    return (
        <div>
            <MyButton buttonClass="btn btn-primary QuantityButton btn-group-lg"
                      buttonHandler={() => onValueChange(amount - 1)} isDisabled={amount === 1} buttonContent="-">
            </MyButton>
            <label>{amount}</label>
            <MyButton buttonClass="btn btn-primary QuantityButton btn-group-lg"
                      buttonHandler={() => onValueChange(amount + 1)} buttonContent="+">
            </MyButton>
        </div>
    );
};

export default QuantityButtons;
