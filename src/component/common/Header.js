import React from 'react';
import logo from "../../static/logo.svg";

const Header = () => (
        <div className="logo-container">
            <img src={logo} className="logo" alt="logo"/>
            <label>React</label>
        </div>
);

export default Header;