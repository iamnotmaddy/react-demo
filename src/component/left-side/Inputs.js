import React, {useState} from 'react';
import '../../static/style/App.css';
import QuantityButtons from "../common/QuantityButtons";
import ProductIconMenu from "../common/ProductIconMenu";
import MyButton from "../common/Button";
import uuid from 'react-uuid';
import {connect} from "react-redux";
import * as actions from '../../actions';

let Inputs = ({dispatch}) => {
    const emptyProduct = {
        name: '',
        price: '',
        amount: 1,
        currentIcon: 0
    };

    const [product, setProduct] = useState(emptyProduct);

    const isAddToListDisabled = () => {
        return product.name === '' || product.price === '';
    };

    const productInputModified = field => e => setProduct({...product, [field]: e.target.value});

    const modifyProductField = (field, value) => {
        setProduct({...product, [field]: value});
    };

    const addToListHandler = () => {
        dispatch(actions.addProductToList(product));
        setProduct(emptyProduct);
    };

    const onAmountChange = (newAmount) => {
        modifyProductField('amount', newAmount);
    };

    const onIconSelected = (iconIndex) => {
        modifyProductField('currentIcon', iconIndex);
    };

    return (
        <div className="Block InputsBlock">
            <span>Add product to your cart list</span>
            <input id="productNameInputId" className="form-control" placeholder="Product name"
                   value={product.name} onChange={productInputModified('name')}/>

            <input id="productPriceInputId" className="form-control" placeholder="Product price"
                   type="number" value={product.price}
                   onChange={productInputModified('price')}/>

            <QuantityButtons key={uuid()}
                             onValueChange={onAmountChange}
                             amount={product.amount}/>

            <ProductIconMenu currentIconIndex={product.currentIcon}
                             onNewIconSelected={onIconSelected}/>

            <MyButton buttonClass="btn btn-primary DefaultButton" buttonContent="Add to list"
                      isDisabled={isAddToListDisabled()} buttonHandler={addToListHandler}
            />
        </div>
    );
};

export default connect()(Inputs);
