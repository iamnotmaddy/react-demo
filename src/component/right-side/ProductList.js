import React, {useEffect, useState} from 'react';
import '../../static/style/App.css';
import ProductEntry from "./ProductEntry";
import {Icons as icons} from "../common/Icons";
import MyButton from "../common/Button";
import {connect} from "react-redux";
import axios from "axios";
import * as actions from '../../actions';

let ProductList = ({dispatch, productEntries}) => {

    useEffect(() => {
        axios.get('https://demo3370310.mockable.io/productData').then(response => {
            dispatch(actions.onDataRequestSuccess(response.data))
        }).catch(error => {
                dispatch(actions.onDataRequestError(error));
            }
        );
    }, []);


    const [showProductInfo, setShowProductInfo] = useState(false);
    const [displayedProduct, setDisplayedProduct] = useState({
        amount: 0,
        price: 0,
        total: 0,
        name: '',
        iconIndex: 0
    });

    const updateProducts = () => {
        return productEntries.map(product => {
            return (
                <ProductEntry productEntry={product} key={product.id} id={product.id}
                              iconIndex={product.currentIcon}
                              onProductInfoClick={onProductInfoClick}
                />
            )
        });
    };

    const onProductInfoClick = productEntry => {
        setDisplayedProduct(productEntry);
        setShowProductInfo(true);
    };

    const countTotalAmount = () => {
        return productEntries.reduce((total, entry) => total + entry.price * entry.amount, 0);
    };

    return (
        showProductInfo ? (
            <div className="Block">
                <span>{displayedProduct.name}</span>

                <div className="Icon" style={{float: 'none'}}>
                    {icons[displayedProduct.iconIndex].icon}
                </div>

                <div>
                    <label>Count: {displayedProduct.amount}</label>
                </div>

                <div>
                    <label>Price: {displayedProduct.price} $</label>
                </div>
                <div>
                    <label>Total: {displayedProduct.total} $</label>
                </div>
                <MyButton buttonClass="btn btn-primary DefaultButton"
                          buttonContent="Back To List" buttonHandler={() => setShowProductInfo(false)}/>
            </div>
        ) : (
            <div className="Block">
                <span>Product list</span>
                {updateProducts()}
                <label style={{float: 'left'}}>Total: {countTotalAmount()} $</label>
            </div>
        ));
};

function mapStateToProps(state) {
    return {
        productEntries: state
    };
}

export default connect(mapStateToProps)(ProductList);



