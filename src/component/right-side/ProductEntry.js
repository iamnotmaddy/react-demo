import React from 'react';
import QuantityButtons from "../common/QuantityButtons";
import {FaTrashAlt} from "react-icons/fa";
import {IoIosLink} from "react-icons/io";
import {Icons as icons} from "../common/Icons";
import MyButton from "../common/Button";
import {connect} from "react-redux";
import * as actions from '../../actions';

let ProductEntry = ({productEntry, iconIndex, onProductInfoClick, id, dispatch}) => {

    const onOpenInfoClick = () => {
        onProductInfoClick({
            ...productEntry,
            total: countTotal(),
            iconIndex: iconIndex
        });
    };

    const countTotal = () => {
        return productEntry.price * productEntry.amount;
    };

    const onAmountChange = newAmount => {
        dispatch(actions.changeAmount(id, newAmount));
    };

    const onDelete = () => {
        dispatch(actions.deleteProductEntry(id));
    };

    return (
        <div className="ProductEntry">
            <label>{productEntry.name}</label>

            <MyButton buttonClass="MyIcon"
                      buttonHandler={onOpenInfoClick}
                      buttonContent={<IoIosLink size={20}/>}>
            </MyButton>
            <MyButton buttonClass="MyIcon"
                      buttonHandler={onDelete}
                      buttonContent={<FaTrashAlt size={20}/>}>
            </MyButton>
            <div className="Icon">
                {icons[iconIndex].icon}
            </div>
            <QuantityButtons amount={productEntry.amount}
                             onValueChange={onAmountChange}
            />
            <label>Total: {countTotal()} $</label>
        </div>
    );
};

export default connect()(ProductEntry);
